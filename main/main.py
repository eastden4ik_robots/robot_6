
from telegram import Bot
from telegram import Update
from telegram import InlineQueryResultArticle, InputTextMessageContent
from telegram.ext import Updater
from telegram.ext import MessageHandler
from telegram.ext import CommandHandler
from telegram.ext import InlineQueryHandler
from telegram.ext import Filters

import logging
import requests
import json
import math

import conf_file


def start(bot: Bot, update: Update):
    bot.send_message(
        chat_id=update.message.chat_id,
        text="Hello to WeatherBot",
    )

def city(bot: Bot, update: Update):
    token = conf_file.WEATHER_TOKEN
    url = 'https://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}'.format(update.message.text.split(" ")[1], token)
    r = requests.get(url)
    print('UserID {0}'.format(update.effective_chat.id))
    tmp = json.loads(r.text)['main']['temp'] - 273.15
    min = json.loads(r.text)['main']['temp_min'] - 273.15
    max = json.loads(r.text)['main']['temp_max'] - 273.15
    hum = json.loads(r.text)['main']['humidity']
    wind = json.loads(r.text)['wind']['speed']
    temp = 'Temparature in {0} is {1}\n' \
           'Min: {2}, Max: {3}\n' \
           'Humidity: {4}\n' \
           'Wind: {5}\n'.format(update.message.text.split(" ")[1], math.floor(tmp), min, max, hum, wind)
    bot.send_message(chat_id=update.effective_chat.id, text=temp)

def unknown(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Sorry, I didn't understand that command.")


def main():

    bot = Bot(
        token=conf_file.TG_TOKEN,
    )

    updater = Updater(
        bot=bot
    )

    dispatcher = updater.dispatcher

    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.DEBUG)

    city_hand = CommandHandler('city', city)
    dispatcher.add_handler(city_hand)

    unknown_handler = MessageHandler(Filters.command, unknown)
    dispatcher.add_handler(unknown_handler)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()